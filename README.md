﻿# CH438

#### 介绍
CH438 for Allwinner A33 platform, 1 to 8 serial ports.
在全志A33平台扩展Ch438Q芯片,它是一个1并转8串的串口扩展芯片.

#### 使用说明
这里讲解的是基于Allwinner A33平台的,其他平台的需自行处理.

1.  将代码放在linux-3.4/drivers/tty/serial目录下
2.  在同目录的Kconfig中加入以下配置:
    config SERIAL_CH438
	bool "Support for ch438 serial port"
	select SERIAL_CORE
	default y
	help
	  If you have an CH438 chip and want to use the built-in UART of
	  the chip, say Y to this option.

3.  在同目录的Makefile中加入以下语句:
    obj-$(CONFIG_SERIAL_CH438)	+= ch438.o
4.  在A33项目的sys_config.fex中加入wiznet配置项:
    [ch438]
    ch438_used = 1
    uart_type  = 2

    ch438_cs = port:PE17<1><1><default><1>
    ch438_rst = port:PF01<1><1><default><1>
    ch438_int = port:PB06<2><default><default><1>
    请根据自己的硬件修改以上IO的配置
5.  完成!
    注意:扩展出来的串口被映射到/dev目录下为ttySC0-ttySC7,
    对应的port->line是0-7,而系统默认的ttyS0(port->line==0)一般是作为debug串口使用的,
    所以不应对扩展的串口检查是否console的判断,否则会导致ttySC0被认为是console而产生
    错误,这是linux内核判断某个串口是否也是console的机制造成的,应用中需要避免!

    如果想让扩展串口也能作为console使用,可以将扩展串口映射为ttyS4-ttyS11,让对应line的
    为4-11.不过ch438内部的port还是0~7,用起来太麻烦!

